# Database Info
![Database Info](./screenshot.jpg) 

## Description
Database Info is a module for Gallery 3 which will allow Gallery 3 administrators to see the size of their gallery 3 database as well as the number of tables in it.

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/95320).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "database_info" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  Once activated you will be able to add the module to the dashboard from the dashboard content box.

## History
**Version 1.1.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 28 August 2021.
>
> Download: [Version 1.1.0](/uploads/7d6324712cd98631b163601c1d57aaad/database_info110.zip)

**Version 1.0.1:**
> - Updated module.info file for changes in Gallery 3.0.2.
> - Released 03 August 2011.
>
> Download: [Version 1.0.1](/uploads/cbc5936b8b5c74cf8708c9c982a439e2/database_info101.zip)

**Version 1.0.0:**
> - Initial Release.
> - Released on 01 April 2010.
>
> Download: [Version 1.0.0](/uploads/076ff3429b8b08192fda804d92ac1cc9/database_info100.zip)
